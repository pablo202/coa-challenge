export class ProductCarts {
	public obj :Object;
	private date :Date = new Date()
	constructor(cartId:string, productId :string) {
		this.obj = {
			product_id: productId,
			cart_id: cartId,
			creation_date: this.date.toDateString() + " " + this.date.getUTCHours() + ":" + this.date.getUTCMinutes() + ":" + this.date.getUTCSeconds() + "hs UTC",
			quantity: 1
		}
	}
}
