import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable, map } from 'rxjs';
import { Cart } from '../models/cart';
import { ProductCarts } from '../models/product-carts';
import Swal from 'sweetalert2';

@Injectable({
	providedIn: 'root'
})
export class FirestoreService {

	constructor(
		private firestore :AngularFirestore
	) {}

	private addProduct(cartId :string, productId:string) {
		return this.firestore.collection(
			"product_carts",
			ref => ref
			.where("cart_id","==",cartId)
			.where("product_id","==",productId)
		)
		.get()
		.subscribe(res => {
			if(res.docs[0] == undefined) {
				this.firestore.collection("product_carts").add(new ProductCarts(cartId,productId).obj);
			} else {
				this.firestore
				.collection("product_carts")
				.doc(res.docs[0].id)
				.update({
					quantity: res.docs[0].get("quantity") + 1
				})
				.then( () => {
					Swal.fire({
						title: "Done",
						text: "The product was added to the cart successfully!",
						icon: "success",
					})
				});
			}
		})
	}

	public searchCart(userId:string) :Observable<any>{
		return this.firestore.collection(
			"carts",
			ref => ref
			.where('id','==', userId)
			.where('status', '==', 'pending')
		)
		.get()
	}


	public getProducts() {
		return this.firestore.collection("products").get();
	}


	public addToCart(userId:string, productId:string) {
		this.searchCart(userId)
		.pipe(
			map(res => {
				if(res.docs[0] == undefined) {
					return this.firestore.collection("carts").add(new Cart(userId).obj)
				} else {
					return res.docs[0].id;
				}
			}),
			map(res => {
				if(typeof res == "string") {
					this.addProduct(res, productId);
				} else {
					res.then((cartId:any) => {
						this.addProduct(cartId.splice(0,6), productId);
					})
				}
			}),
		)
		.subscribe()
	}
	public getCartProductData(productId:string) :Observable<any> {
		return  this.firestore.collection("products").doc(productId).get();
	}
	public decreaseProductQuantityFromCart(productId:string, quantity:number) :void {
		this.firestore.collection("product_carts").doc(productId).update({
			quantity: quantity - 1
		})
		.then( () => {
			Swal.fire({
				title: "Done",
				text: "The product was removed from the cart successfully!",
				icon: "success",
			})
		});
	}
	public removeProductFromCart(productId:string) :void{
		this.firestore.collection("product_carts").doc(productId).delete()
		.then( () => {
			Swal.fire({
				title: "Done",
				text: "The product was removed from the cart successfully!",
				icon: "success",
			})
		});
	}

	public getCartProducts(cartId:string) :Observable<any>{
		return this.firestore.collection(
			"product_carts",
			ref => ref
			.where("cart_id", "==", cartId)
			.orderBy("creation_date", "asc")
		)
		.snapshotChanges();
	}

	public getCart(userId:string) :Observable<any>{
		return this.searchCart(userId)
	}


}
