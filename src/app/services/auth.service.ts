import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router'

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(
		private auth :AngularFireAuth,
		private router :Router
	) { }

	public register(email:string, password:string) :void{
		this.auth.createUserWithEmailAndPassword(email,password)
		.then((response) => {
			sessionStorage.setItem("userId", response.user?.uid || "");
			this.router.navigate([""]);
		});
	}

	public logIn(email:string, password:string) :void {
		this.auth.signInWithEmailAndPassword(email, password)
		.then((response) => {
			sessionStorage.setItem("userId", response.user?.uid || "");
			this.router.navigate(["products"]);
		});
	}

	public signOut() {
		this.auth.signOut();
		this.router.navigate([""]);
	}

}
