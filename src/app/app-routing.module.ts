import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { LoginComponent } from './components/login/login.component';
import { ProductsComponent } from './components/products/products.component';
import { RegisterComponent } from './components/register/register.component';

import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/compat/auth-guard'; 

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo([""]);

const routes: Routes = [
	{
		path: "products",
		component: ProductsComponent,
		canActivate: [AngularFireAuthGuard],
		data: {authGuardPipe: redirectUnauthorizedToLogin}
	},
	{
		path: "cart",
		component: CartComponent,
		canActivate: [AngularFireAuthGuard],
		data: {authGuardPipe: redirectUnauthorizedToLogin}
	},
	{
		path: "register",
		component: RegisterComponent
	},
	{
		path: "**",
		component: LoginComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
