import { Component, OnInit, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore.service';



@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, AfterContentChecked {
	private userId = sessionStorage.getItem("userId") || "";
	public loading :boolean = true;
	public list :Array<any>= [];
	private loadedProducts :number = 0;
	
	ngAfterContentChecked(): void {
	    this.changeDetector.detectChanges();
	}

	public loadCount() {
		if(this.loadedProducts < this.list.length - 1) {
			this.loadedProducts++;
		} else {
			this.loading = false;
		}
	}

	public startLoad() {
		this.loading = true;
		this.loadedProducts = 0;
	}

	constructor(
		private firestore :FirestoreService,
		private changeDetector :ChangeDetectorRef
	) { }

	ngOnInit(): void {
		this.firestore.getCart(this.userId)
		.subscribe(
			cart => {
				this.firestore.getCartProducts(cart.docs[0].id)
				.subscribe(
					docs => {
						this.list = [];
						docs.forEach((element:any) => {
							this.list.push({
								id: element.payload.doc.id,
								...element.payload.doc.data()
							})
						});
					}
				)
			}
		)
	}

}
