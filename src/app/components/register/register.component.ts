import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms'
import { FormlyFieldConfig } from '@ngx-formly/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
	public registerForm = new FormGroup({});
	public fields: FormlyFieldConfig[] = [
		{
			key: 'email',
			type: 'input',
			templateOptions: {
				label: 'Email address',
				placeholder: 'Enter email',
				required: true,
			}
		},
		{
			key: 'password',
			type: 'input',
			templateOptions: {
				label: 'Password',
				type: 'password',
				placeholder: 'Enter password',
				required: true,
			}
		}
	];

	constructor(
		private auth :AuthService
	) {

	}

	public submit() :void {
		this.auth.register(
			this.registerForm.get("email")?.value,
			this.registerForm.get("password")?.value
		);
	}

	ngOnInit(): void {

	}
}
