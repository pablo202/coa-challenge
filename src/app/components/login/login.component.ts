import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	public logInForm = new FormGroup({});
	public fields: FormlyFieldConfig[] = [
		{
			key: 'email',
			type: 'input',
			templateOptions: {
				label: 'Email address',
				placeholder: 'Enter email',
				required: true,
			}
		},
		{
			key: 'password',
			type: 'input',
			templateOptions: {
				label: 'Password',
				type: 'password',
				placeholder: 'Enter password',
				required: true,
			}
		}
	];
	constructor(
		private auth :AuthService,
	) {
	
	}

	public submit() :void{
		this.auth.logIn(
			this.logInForm.get("email")?.value,
			this.logInForm.get("password")?.value
		)
	}

	ngOnInit(): void {
	}

}
