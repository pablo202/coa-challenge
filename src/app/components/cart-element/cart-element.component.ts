import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore.service';
import Swal from 'sweetalert2';

@Component({
	selector: '[my-tr]',
	templateUrl: './cart-element.component.html',
	styleUrls: ['./cart-element.component.scss']
})
export class CartElementComponent implements OnInit {
	@Input() data :any;
	@Output() loaded = new EventEmitter<void>();
	@Output() load = new EventEmitter<void>();
	private userId = sessionStorage.getItem("userId") || "";
	public name = "";
	public price = 0;
	constructor(
		private firestore :FirestoreService
	) { }

	public delete() :void{
		Swal.fire({
			title: 'Are you sure?',
			text: 'The product will be removed from the shopping cart',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Remove',
			reverseButtons: true
		}).then(res => {
			if(res.isConfirmed) {
				this.load.emit();
				if( this.data.quantity == 1) {
					this.firestore.removeProductFromCart(this.data?.id);
				} else {
					this.firestore.decreaseProductQuantityFromCart(this.data?.id, this.data?.quantity);
				}
			}
		});
		
	}
	public addToCart() :void{
		Swal.fire({
			title: 'Are you sure?',
			text: 'The product will be added to the shopping cart',
			icon: 'question',
			showCancelButton: true,
			confirmButtonText: 'Add',
			reverseButtons: true
		}).then(res => {
			if(res.isConfirmed) {
				this.load.emit();
				this.firestore.addToCart(this.userId, this.data?.product_id);
			}
		});
	}

	ngOnInit(): void {
		this.firestore.getCartProductData(this.data?.product_id)
		.subscribe(res => {
			this.name = res.data().name;
			this.price = res.data().price;
			this.loaded.emit();
		})
	}

}
