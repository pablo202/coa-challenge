import { Component, OnInit } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
	public list :Array<any> = [];
	private userId :string = sessionStorage.getItem("userId") || "";

	constructor(
		private firestore :FirestoreService
	) {}
	public addToCart(productId :any) :void{
		Swal.fire({
			title: 'Are you sure?',
			text: 'The product will be added to the shopping cart',
			icon: 'question',
			showCancelButton: true,
			confirmButtonText: 'Add',
			reverseButtons: true
		}).then(res => {
			if(res.isConfirmed) {
				this.firestore.addToCart(this.userId, productId);
			}
		});
	}
	ngOnInit(): void {
		this.firestore.getProducts()
		.subscribe({
			next: (querySnapshot) => {
				querySnapshot.forEach((res) => this.list.push(res.data()));
			}
		});
	}
}
