// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebaseConfig: {
		apiKey: "AIzaSyB6QkyGvcyvNUKAISaXBTvaqlR892dts2Y",
		authDomain: "coa-challenge-6cdc3.firebaseapp.com",
		projectId: "coa-challenge-6cdc3",
		storageBucket: "coa-challenge-6cdc3.appspot.com",
		messagingSenderId: "55869956396",
		appId: "1:55869956396:web:c519c70ea524988e47cc89"
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
